class FoodModel {
  int? id;
  String name;
  String price;
  String image;

  FoodModel({
    this.id,
    required this.name,
    required this.price,
    required this.image,
  });

  factory FoodModel.fromMap(Map<String, dynamic> json) => new FoodModel(
        id: json['id'],
        name: json['name'],
        price: json['price'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'image': image,
    };
  }
}
