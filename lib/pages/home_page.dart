import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/data_model.dart';
import 'package:project_sqlite/model/food_model.dart';
import 'package:project_sqlite/pages/about.dart';
import 'package:project_sqlite/pages/add_food.dart';
import 'package:project_sqlite/pages/edit_food.dart';
import 'package:project_sqlite/pages/show_food.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              SystemNavigator.pop();
            },
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<List<FoodModel>>(
            future: DatabaseHelper.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<FoodModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Favorite Food in List.'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Center(
                          child: Card(
                            color: selectedId == grocery.id
                                ? Colors.white70
                                : Colors.white,
                            child: ListTile(
                              title: Text('${grocery.name} '),
                              subtitle: Text('${grocery.price} บาท'),
                              leading: CircleAvatar(
                                  backgroundImage:
                                      FileImage(File(grocery.image))),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditFood(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "Do you want to delete this record?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShowFood(id: profileid)));

                                setState(() {
                                  print(grocery.image);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  DatabaseHelper.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(50, 75, 205, 1),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
            children: <Widget>[
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'Favorite Food',
                icon: Icons.food_bank,
                onClicked: () => selectedItem(context, 0),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'Add Favorite Food',
                icon: Icons.food_bank_outlined,
                onClicked: () => selectedItem(context, 1),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'About',
                icon: Icons.people,
                onClicked: () => selectedItem(context, 2),
              ),
              const SizedBox(
                height: 16,
              ),
              buildMenuItem(
                text: 'Close',
                icon: Icons.close,
                onClicked: () => selectedItem(context, 0),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;
      case 1:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddFood()),
        );
        break;
      case 2:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const About()),
        );
        break;
    }
  }
}
